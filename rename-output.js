var fs = require('fs');
//delete old ones
if (fs.existsSync('public/css/app.css')) {
	fs.unlinkSync('public/css/app.css');
}

if (fs.existsSync('public/js/app.js')) {
    fs.unlinkSync('public/js/app.js');
}

if (fs.existsSync('public/css/admin/app.css')) {
    fs.unlinkSync('public/css/admin/app.css');
}

if (fs.existsSync('public/js/admin/app.js')) {
    fs.unlinkSync('public/js/admin/app.js');
}

if (fs.existsSync('public/js/admin/libs/tinymce.js')) {
    fs.unlinkSync('public/js/admin/libs/tinymce.js');
}

if (fs.existsSync('public/js/admin/libs/onleave.js')) {
    fs.unlinkSync('public/js/admin/libs/onleave.js');
}



//get file names
cssFileName = fs.readdirSync('public/css').filter(file => {
	return file.includes("app");
});

jsFileName = fs.readdirSync('public/js').filter(file => {
	return file.includes("app");
});

adminCss = fs.readdirSync('public/css/admin').filter(file => {
	return file.includes("app");
});

adminJs = fs.readdirSync('public/js/admin').filter(file => {
	return file.includes("app");
});

tinymceJs = fs.readdirSync('public/js/admin/libs').filter(file => {
	return file.includes("tinymce");
});

onleaveJs = fs.readdirSync('public/js/admin/libs').filter(file => {
	return file.includes("onleave");
});


//rename them
fs.renameSync('public/css/' + cssFileName, 'public/css/app.css');
fs.renameSync('public/js/' + jsFileName, 'public/js/app.js');
fs.renameSync('public/css/admin/' + adminCss, 'public/css/admin/app.css');
fs.renameSync('public/js/admin/' + adminJs, 'public/js/admin/app.js');
fs.renameSync('public/js/admin/libs/' + tinymceJs, 'public/js/admin/libs/tinymce.js');
fs.renameSync('public/js/admin/libs/' + onleaveJs, 'public/js/admin/libs/onleave.js');