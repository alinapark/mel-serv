$(document).ready(function() {
    var total = $(".total_number_of_comments").val();
    var i;
    for (i = 1; i <= total; i++) {
        var height1 = $("#vop_comment" + i).height();
        var height2 = $("#psycho_comment" + i).height();
        if (height1 > height2) {
            $(".input __js-tinymce").height(height1);
            $("#vop_comment" + i).height(height1);
            $("#psycho_comment" + i).height(height1);
        } else {
            $(".input __js-tinymce").height(height2);
            $("#vop_comment" + i).height(height2);
            $("#psycho_comment" + i).height(height2);
        }
    }

    var buttons = document.querySelectorAll(".toggle_print");
    var modal = document.querySelector("#modal_chat");

    [].forEach.call(buttons, function(button) {
        button.addEventListener("click", function() {
            modal.classList.toggle("off");
        })
    });

    // $(".toggle_button").click(function () {
    //     modal.classList.toggle("off");
    //
    //     /**
    //      * Generates PDF file containing messages from doctor or psychiatrist roles.
    //      *
    //      * @param studentId the student id
    //      * @param groupKey the group key ("doctor" or "psychiatrist")
    //      * @param pages the string representing pages ex "1,2-4"
    //      * @return generated PDF file
    //      */
    //     var student_code = document.querySelector('.divider').id;
    //
    //     var val = $("input[name=groupKey]:checked").val();
    //     var pages_string = $("#pages_id").val();
    //
    //     var finurl = "downloadMessages?studentId="+student_code+"&groupKey="+val+"&pages="+pages_string;
    //
    //     var win = window.open(finurl, '_blank');
    //     win.focus();
    //
    // });

    $(".toggle_print").click(function() {
        $(".modal-overlay").show();
        $("#ask-modal").css('display', 'flex');
    });

    $("#ask-modal-close").click(function() {
        $(".modal-overlay").hide();
        $("#ask-modal").hide();
    });

    $('#send-button').click(function(event) {
        // enter has keyCode = 13, change it if you want to use another button
        $("#modal-success").hide();
        $("#modal-failure").hide();

        var student_id = document.querySelector('.divider').id;

        var val = $("input[name=groupKey]:checked").val();
        if (!val) {
            return;
        }
        var pages_string = $("#pages_id").val();

        var finurl = "/leading/Print?studentId="+student_id+"&groupKey="+val+"&pages="+pages_string;
        var win = window.open(finurl, '_blank');
        win.focus();
    });

});


$(window).scroll(function(e) {
    // Get the position of the location where the scroller starts.
    var scroller_anchor = $(".scroller_anchor").offset().top;

    // Check if the user has scrolled and the current position is after the scroller start location and if its not already fixed at the top
    if ($(this).scrollTop() >= scroller_anchor && $('.scroller').css('position') != 'fixed')
    {    // Change the CSS of the scroller to hilight it and fix it at the top of the screen.
        $('.scroller').css({
            'background': '#4f9758',
            'color':'white',
            'border': 'none',
            'font-family': 'Roboto',
            'font-weight': 'bold',
            'position': 'fixed',
            'top': '0px'
        });
        // Changing the height of the scroller anchor to that of scroller so that there is no change in the overall height of the page.
        $('.scroller_anchor').css('height', '50px');
    }
    else if ($(this).scrollTop() < scroller_anchor && $('.scroller').css('position') != 'relative')
    {    // If the user has scrolled back to the location above the scroller anchor place it back into the content.

        // Change the height of the scroller anchor to 0 and now we will be adding the scroller back to the content.
        $('.scroller_anchor').css('height', '0px');

        // Change the CSS and put it back to its original position.
        $('.scroller').css({
            'background': '#4f9758',
            'color':'white',
            'border': 'none',
            'font-family': 'Roboto',
            'font-weight': 'bold',
            'position': 'relative'
        });
    }

});