$(function () {
    $(document).scrollTop(0);

    var carousel_1 = $('#carousel-1').owlCarousel({
        loop: true,
        navText: [''],
        margin: 0,
        items: 1
    });

    $('.carousel-block__nav-prev').on('click', function () {
        carousel_1.trigger('prev.owl.carousel');
    });
    $('.carousel-block__nav-next').on('click', function () {
        carousel_1.trigger('next.owl.carousel');
    });


    //if ($('#header-float').length > 0) {
    //    $(document).on('scroll', function () {
    //        if ($(this).scrollTop() > $('#header-float').outerHeight() + 50) {
    //            $('#header-float').addClass('__fixed');
    //            $('.header-float__space-wrapper').css('height', 82 + 'px');
    //        } else {
    //            $('#header-float').removeClass('__fixed');
    //            $('.header-float__space-wrapper').css('height', 0);
    //        }
    //    });
    //}

    if ($(window).width() > 1199) {
        //Parallax
        $('.slide-block__information-wrapper, .video-block-wrapper, .carousel-block').scroolly([
            {
                from: 'el-top = vp-center + 30vp',
                to: 'el-center = vp-center',
                cssFrom: {
                    opacity: '.0',
                    top: '100px'
                },
                cssTo: {
                    opacity: '1',
                    top: '0'
                }
            }

        ]);

        $('#slide-block-1').scroolly([
            {
                from: 'el-top = vp-center',
                to: 'el-bottom = vp-top',
                onScroll: function (element, offset, length) {
                    var progress = offset / length;
                    element.css('background-position', 'center ' + $.scroolly.getTransitionFloatValue(-15, 40, progress) + '%');
                }
            }

        ]);

        $('#slide-block-2, #slide-block-4, #slide-block-5').scroolly([
            {
                from: 'el-top = vp-center',
                to: 'el-bottom = vp-top',
                onScroll: function (element, offset, length) {
                    var progress = offset / length;
                    element.css('background-position', 'center ' + $.scroolly.getTransitionFloatValue(0, 80, progress) + '%');
                }
            }

        ]);

        $('#slide-block-2-2').scroolly([
            {
                from: 'el-top = vp-center',
                to: 'el-bottom = vp-top',
                onScroll: function (element, offset, length) {
                    var progress = offset / length;
                    element.css('background-position', 'left ' + $.scroolly.getTransitionFloatValue(50, -100, progress) + 'px');
                }
            }

        ]);

        $('#slide-block-3').scroolly([
            {
                from: 'el-top = vp-center',
                to: 'el-bottom = vp-top',
                onScroll: function (element, offset, length) {
                    var progress = offset / length;
                    element.css('background-position', 'center ' + $.scroolly.getTransitionFloatValue(70, 95, progress) + '%');
                }
            }

        ]);


        //spy menu
        $('.slide-block').scroolly([
            {
                from: 'con-top = vp-top',
                to: 'con-bottom = vp-top',
                onCheckIn: function ($el) {
                    $('ul.nav-menu li[data-target]').removeClass('is-active');
                    $('ul.nav-menu li[data-target="' + $el.data('item') + '"]').addClass('is-active');
                },
                onCheckOut: function ($el) {
                    $('.navbar li[data-target]').removeClass('is-active');
                }

            }
        ]);
        $('ul.nav-menu li').on('click', function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop: $('[data-item="' + $(this).data('target') + '"]').offset().top}, 500);
        });
    }

});
