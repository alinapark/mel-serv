$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});

let accountable = {
    group: {
        block: $('#field-group'),
        field: $('#field-group select')
    },
    parent: {
        block: $('#field-parent'),
        field: $('#field-parent select')
    }
};

accountable.group.field.on('change', function () {
    if (!$(this).val() || $(this).val() != 1) {
        accountable.parent.block.addClass('is-hidden');
    } else {
        accountable.parent.block.removeClass('is-hidden');
    }
});