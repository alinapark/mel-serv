var quiz = {
    disabled: 'is-disabled',
    init: function() {
        $(".questions").sortable({
            update: function () {
                quiz.question.reorder();
            }
        }).disableSelection();

        quiz.question.recount();
        quiz.question.reorder();

        $('.quiz').find('.question').each(function () {
            quiz.answer.recount($(this));
            quiz.answer.reorder($(this));
        });
    },
    recount: function (count, buttons, limits) {
        buttons.create.removeClass(quiz.disabled);
        buttons.remove.removeClass(quiz.disabled);

        if (count <= limits.min) {
            buttons.remove.addClass(quiz.disabled);
        } else if (count >= limits.max) {
            buttons.create.addClass(quiz.disabled);
        }
    },
    question: {
        recount: function () {
            quiz.recount(
                $('.quiz .question').length,
                {
                    create: $('.__question-create'),
                    remove: $('.__question-remove')
                },
                {
                    min: 1,
                    max: 75
                }
            );
        },
        reorder: function () {
            $('.quiz').find('.question').each(function (i) {
                $(this).find('.answers').attr('index', i).end()
                quiz.answer.reorder($(this));


                $(this).find('input[data-question=true]').each(function () {
                    $(this).attr('name', `questions[${i}]`)
                });

                $(this).find('textarea').each(function () {
                    $(this).attr('name', `descriptions[${i}]`)
                });
            });
        },
        create: function (object) {
            object = object.parent().parent().parent();
            var cloned = object.clone(true).insertAfter(object)
                .find('.answer:gt(2)').remove().end()
                .find('input[type=text], textarea').val('').end()
                .find('button').removeClass(quiz.disabled).end()
                .find('input[type=checkbox]').removeAttr('checked').end();




            // if (cloned.find('.answer').length > 0) {
                // var answer = cloned.find('.answer:last-child');
                // answer.clone(true).insertAfter(answer).attr('name');
            cloned.find('.answers').attr('index', parseInt(cloned.find('.answers').attr('index')) + 1);
            quiz.answer.reorder(cloned);
            // }

            quiz.question.recount();
            quiz.question.reorder();

            
        },
        remove: function (object) {
            if (confirm('Вы уверены, что хотите удалить данный вопрос?')) {
                object.parent().parent().parent().remove();

                quiz.question.recount();
                quiz.question.reorder();
            }
        }
    },
    answer: {
        recount: function (object) {
            quiz.recount(
                object.find('.answer').length,
                {
                    create: object.find('.__answer-create'),
                    remove: object.find('.__answer-remove'),
                },
                {
                    min: 2,
                    max: 10
                }
            );
        },
        reorder: function (object) {
            object.find('.answer').each(function (i) {
                var index = $(this).parent().attr('index');
                $(this).find('input[type=text]').each(function () {
                    $(this).attr('name', `answers[${index}][${i}]`)
                });

                $(this).find('input[type=checkbox], input[type=hidden]').each(function () {
                    $(this).attr('name', `isright[${index}][${i}]`)
                });
            });
        },
        create: function (object) {
            object = object.parent().parent();
            object.clone(true).insertAfter(object)
                .find('input[type=text]').val('').end()
                .find('input[type=checkbox]').removeAttr('checked');

            var parent = object.parent().parent();

            quiz.answer.recount(parent);
            quiz.answer.reorder(parent);
        },
        remove: function (object) {
            if (confirm('Вы уверены, что хотите удалить данный ответ?')) {
                var answer = object.parent().parent();
                var parent = answer.parent().parent();

                answer.remove();

                quiz.answer.recount(parent);
                quiz.answer.reorder(parent);
            }
        }
    }
}

quiz.init();

$('.quiz .__question-create').on('click', function () {
    if ( ! $(this).hasClass(quiz.disabled)) {
        quiz.question.create($(this));
    }
});

$('.quiz .__question-remove').on('click', function () {
    if ( ! $(this).hasClass(quiz.disabled)) {
        quiz.question.remove($(this));
    }
});

$('.quiz .__answer-create').on('click', function () {
    if ( ! $(this).hasClass(quiz.disabled)) {
        quiz.answer.create($(this));
    }
});

$('.quiz .__answer-remove').on('click', function () {
    if ( ! $(this).hasClass(quiz.disabled)) {
        quiz.answer.remove($(this));
    }
});

$('.quiz input[type=checkbox]').on('change', function() {
    $(this).parents('.question').find('input[type=checkbox]').prop('checked', false);
    $(this).prop('checked', true);
});
