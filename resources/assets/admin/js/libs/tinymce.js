require('../../../../../node_modules/jquery-form/jquery.form');

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
});

let tinyMce = {
    form : $('.__js-tinymce__form'),
    file : $('.__js-tinymce__file')
}

tinyMce.file.change(function () {
    tinyMce.form.ajaxSubmit(function(response) {
        $('body').append(response);
    });

    tinyMce.file.val('');
});

tinymce.init({
    menu: {},
    selector: '.__js-tinymce',
    statusbar: false,
    plugins: ['link', 'code', 'table', 'autoresize', 'image'],
    toolbar: 'fontsizeselect h1 bold italic underline | removeformat code | table tablecontrols bullist numlist link image',
    fontsize_formats: '8pt 10pt 12pt 14pt 18pt 24pt 36pt',
    autoresize_bottom_margin: 50,
    autoresize_min_height: 200,
    autoresize_max_height: 600,
    setup: function(editor) {
        editor.on('change', function() {
            window.forWasChanged = true;
        });
    },
    file_picker_callback: function (callback, value, meta) {
        if (meta.filetype == 'image') {
            tinyMce.file.click();
        }
    }
});
