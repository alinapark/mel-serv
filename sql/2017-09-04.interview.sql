CREATE TABLE interview_results (
    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    verdict enum('approved', 'refuted') DEFAULT 'approved',
    text_ text,
    risk enum('high', 'presents', 'low', 'no') DEFAULT 'no',
    student_id int(10) UNSIGNED,
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    FOREIGN KEY (student_id) REFERENCES students(id) ON DELETE CASCADE ON UPDATE CASCADE,
    PRIMARY KEY (id)
);

ALTER TABLE survey_results add column interview_result int(10) UNSIGNED;
ALTER TABLE survey_results add foreign key (interview_result) references interview_results(id);