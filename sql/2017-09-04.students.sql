CREATE TABLE students (
    id int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    fullname varchar(255) NOT NULL,
    code varchar(255) NOT NULL,
    creator_id int(10) UNSIGNED,
	school_id int(11) DEFAULT NULL,
    created_at TIMESTAMP NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMP NOT NULL DEFAULT NOW(),
    deleted_at TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (creator_id) REFERENCES accounts(id)
);

alter table students add column gender enum('m','f') COLLATE utf8mb4_unicode_ci NOT NULL;
alter table students add column birthdate TIMESTAMP NULL DEFAULT NULL;
alter table students add column status enum('created','finished','interrupted') COLLATE utf8mb4_unicode_ci NOT NULL;

alter table news MODIFY column group_id int(10) unsigned null;