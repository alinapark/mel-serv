use `zhastar`;

alter table zhastar.accounts drop column `parent_id`;
alter table zhastar.accounts drop column `generated_id`;
alter table zhastar.accounts drop column `remember_token`;
alter table zhastar.profiles drop column `sex`;
alter table zhastar.quiz_scores drop column `total`;
alter table zhastar.users drop column `region_id`;
alter table zhastar.users drop column `province_id`;
alter table zhastar.users drop column `city_id`;
alter table zhastar.users drop column `remember_token`;
alter table zhastar.users drop column `is_moder`;

alter table zhastar.news change `new_type_id` `news_type_id` int(10) unsigned NOT NULL;
ALTER TABLE zhastar.news DROP FOREIGN KEY `news_ibfk_2`;

rename table `new_types` to `news_types`;
rename table `users` to `admins`;

ALTER TABLE zhastar.news ADD constraint news_ibfk_2 FOREIGN KEY (`news_type_id`) REFERENCES `news_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

drop table `languages`;
drop table `migrations`;
drop table `progresses`;
drop table `translations`;
drop table `works`;