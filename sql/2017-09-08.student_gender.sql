alter table students add column gender enum('m','f');
update students s set s.gender = 'm' where s.id mod 2 = 0;
update students s set s.gender = 'f' where s.id mod 2 = 1;