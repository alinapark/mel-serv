delete from accounts where group_id not in (select id from groups);
delete from steps where group_id not in (select id from groups);
delete from documents where group_id not in (select id from groups);
delete from lessons where group_id not in (select id from groups);
delete from quizzes where group_id not in (select id from groups);