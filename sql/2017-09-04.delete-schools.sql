delete from schools where `region_id` not in (select id from locations);
delete from schools where `country_id` not in (select id from locations);
delete from schools where `city_id` not in (select id from locations);
delete from schools where `province_id` not in (select id from locations);