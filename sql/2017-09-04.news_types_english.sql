alter table new_types add column name_en varchar(255);
update new_types set name_en = 'Instructions' where id = 1;
update new_types set name_en = 'Literature' where id = 2;
update new_types set name_en = 'News' where id = 3;
