CREATE TABLE `supervisors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `region_id` int(11) DEFAULT NULL,
  `province_id` int(11) DEFAULT NULL,
  `type_` enum('republic', 'region', 'province') DEFAULT 'republic',
  PRIMARY KEY (`id`),
  UNIQUE KEY `supervisors_login_unique` (`login_`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;