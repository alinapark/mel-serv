package controllers;

import models.Account;
import models.AdminUser;
import models.Profile;
import play.i18n.Lang;
import play.mvc.*;
import utils.BCrypt;

public class Application extends Controller {

    public static void index() {
        renderArgs.put("home", true);
        if (session.get("logged") == null) {
            render();
        } else {
            Pages.contentDashboard(null);
        }
    }

    public static void changeLang(String lang) {
        Lang.change(lang);
        index();
    }

    public static void login() {
        if (flash.get("error") != null) {
            renderArgs.put("error", true);
        }
        if (session.get("logged") == null) {
            Boolean headerContent = true;
            render(headerContent);
        } else {
            Pages.contentDashboard(null);
        }
    }

    public static void authenticate(String username, String password) {
        if (username != null) {
            Account account = Account.find("username = ?1", username).first();
            if (account != null) {
                if (BCrypt.checkpw(password, account.password)) {
                    session.put("logged", username);
                    Profile profile = Profile.find("account = ?1", account).first();
                    if (profile == null) {
                        Settings.profile();
                    }
                    Pages.contentDashboard(null);
                }
            }
        }
        flash.put("error", true);
        login();
    }

    public static void adminLogin() {
        String errorMessage = flash.get("error");
        String active = "login";
        if (session.get("adminLogged") != null) {
            controllers.Admin.accounts(1, null, null, null, null, null, null);
        }
        renderTemplate("admin/auth/login.html", active, errorMessage);
    }

    public static void adminAuthenticate(String email, String password) {
        if (email != null) {
            AdminUser admin = AdminUser.find("email = ?", email).first();
            if (admin != null) {
                if (BCrypt.checkpw(password, admin.password)) {
                    session.put("adminLogged", admin);
                    controllers.Admin.accounts(1, null, null, null, null, null, null);
                }
            }
        }
        String errorMessage = "Имя пользователя и пароль не совпадают.\n";
        flash.put("error", errorMessage);
        adminLogin();
    }

    public static void adminLogout() {
        session.remove("adminLogged");
        adminLogin();
    }

    public static void about() {
        renderArgs.put("headerContent", true);
        renderArgs.put("username", session.get("logged"));
        render();
    }

    public static void logout() {
        session.remove("logged");
        index();
    }
}
