package controllers;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.itextpdf.text.DocumentException;
import dto.SurveyAnswersDto;
import enums.Gender;
import models.*;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.math.NumberUtils;
import play.Logger;
import play.data.validation.Required;
import play.data.validation.Validation;
import play.db.jpa.JPA;
import play.db.jpa.JPABase;
import play.mvc.Before;
import play.templates.Template;
import play.templates.TemplateLoader;
import utils.PdfCreator;
import utils.SurveyUtils;

import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sagynysh on 7/4/17.
 */
public class SurveyController extends SecureController {

    private static final int N = 4819;
    private static final int E = 73;

    @Before(priority = 1, unless = {"survey", "surveyQuestions", "interruptSurvey", "submitResults"})
    public static void checkConditions() {
        Account user = renderArgs.get("user", Account.class);
        if (user.group.key.equals("psychiatrist") || user.group.key.equals("doctor")) {
            if (!request.action.equals("SurveyController.student") && !request.action.equals("SurveyController.downloadAnswers")) {
                Pages.contentDashboard(null);
            }
        } else {
            if (!user.group.key.equals("psychologist")) {
                Pages.contentDashboard(null);
            }
        }
        renderArgs.put("headerContent", true);
    }

    public static void index(String code, Integer type) {
        renderArgs.put("newStudent", flash.get("newStudent"));
        Account user = renderArgs.get("user", Account.class);
        List<Student> students = null;
        if (code == null && type == null) {
            students = Student.find("creator = ?1 and deletedAt is null", user).fetch();
        } else if (code != null && !code.trim().equals("")) {
            renderArgs.put("code", code);
            code = "%" + code.trim().replaceAll("-", "") + "%";
            students = Student.find("creator = ?1 and deletedAt is null and code like ?2", user, code).fetch();
        } else if (type != null) {
            renderArgs.put("type", type);
            switch (type) {
                case 2:
                    students = JPA.em().createQuery("Select a from Student a where " +
                            "a.deletedAt is null and a.creator = ?1 and a.status = ?2", Student.class)
                            .setParameter(1, user)
                            .setParameter(2, Student.Status.created)
                            .getResultList();
                    break;
                case 3:
                    students = JPA.em().createNativeQuery("Select s.*" +
                            " from students s left join survey_results r on s.id = r.student_id" +
                            " where s.deleted_at is null and s.creator_id = ?1" +
                            " and ((r.is_in_risk_group = true and r.is_suicide_heavy = true) or (s.status = 'interrupted'))", Student.class).setParameter(1, user.id).getResultList();
                    break;
                case 4:
                    students = JPA.em().createNativeQuery("Select s.*" +
                            " from students s, survey_results r" +
                            " where s.id = r.student_id and s.deleted_at is null and s.creator_id = ?1" +
                            " and r.is_in_risk_group = true and r.is_suicide_heavy = false", Student.class).setParameter(1, user.id).getResultList();
                    break;
                case 5:
                    students = JPA.em().createNativeQuery("Select s.*" +
                            " from students s, survey_results r" +
                            " where s.id = r.student_id and s.deleted_at is null and s.creator_id = ?1" +
                            " and r.is_in_risk_group = false and r.prosocial_behavior_scale <= 2", Student.class).setParameter(1, user.id).getResultList();
                    break;
                case 6:
                    students = JPA.em().createNativeQuery("Select s.*" +
                            " from students s, survey_results r" +
                            " where s.id = r.student_id and s.deleted_at is null and s.creator_id = ?1" +
                            " and r.is_in_risk_group = false and r.prosocial_behavior_scale > 2", Student.class).setParameter(1, user.id).getResultList();
                    break;
                default:
                    students = Student.find("creator = ?1 and deletedAt is null", user).fetch();
                    break;
            }
        }
        BigInteger didNotTryCount = (BigInteger) JPA.em().createNativeQuery("Select count(*) from students s where s.deleted_at is null and s.creator_id = ?1 and s.status = 'created'").setParameter(1, user.id).getSingleResult();
        BigInteger highRiskCount = (BigInteger) JPA.em().createNativeQuery("Select count(*) from students s left join survey_results r on s.id = r.student_id where s.deleted_at is null and s.creator_id = ?1 and ((r.is_in_risk_group = true and r.is_suicide_heavy = true) or (s.status = 'interrupted'))").setParameter(1, user.id).getSingleResult();
        BigInteger riskCount = (BigInteger) JPA.em().createNativeQuery("Select count(*) from students s left join survey_results r on s.id = r.student_id where s.deleted_at is null and s.creator_id = ?1 and r.is_in_risk_group = true and r.is_suicide_heavy = false").setParameter(1, user.id).getSingleResult();
        BigInteger lowRiskCount = (BigInteger) JPA.em().createNativeQuery("Select count(*) from students s left join survey_results r on s.id = r.student_id where s.deleted_at is null and s.creator_id = ?1 and r.is_in_risk_group = false and r.prosocial_behavior_scale <= 2").setParameter(1, user.id).getSingleResult();
        BigInteger noRiskCount = (BigInteger) JPA.em().createNativeQuery("Select count(*) from students s left join survey_results r on s.id = r.student_id where s.deleted_at is null and s.creator_id = ?1 and r.is_in_risk_group = false and r.prosocial_behavior_scale > 2").setParameter(1, user.id).getSingleResult();
        Long allCount = Student.count("creator = ?1 and deletedAt is null", user);
        render(students, didNotTryCount, highRiskCount, riskCount, lowRiskCount, noRiskCount, allCount);
    }

    public static void interviewResults(Integer type, String code) {
        Account user = renderArgs.get("user", Account.class);
        List<Student> students = new ArrayList<Student>();
        if (code != null && !code.trim().equals("")) {
            String cleanCode = "%" + code.trim().replaceAll("-", "") + "%";
            List<Student> studentsFound = Student.find("creator = ?1 and deletedAt is null and code like ?2", user, cleanCode).fetch();
            for (Student student: studentsFound) {
                boolean needed = true;
                if (student.status.equals(Student.Status.created)) {
                    needed = false;
                }
                if (student.status.equals(Student.Status.finished) && student.results.get(0).risk().equals(SurveyResult.Risk.no)) {
                    needed = false;
                }
                if (needed) {
                    students.add(student);
                }
            }
            renderTemplate("SurveyController/interviewResults.html", students, code);
        }
        List<Student> studentsFound = null;
        studentsFound = Student.find("creator = ?1 and deletedAt is null and status != ?2", user, Student.Status.created).fetch();
        int allCount = 0;
        int noResultCount = 0;
        int approvedCount = 0;
        int refutedCount = 0;
        for (Student student: studentsFound) {
            boolean needed = true;
            if (student.status.equals(Student.Status.finished)) {
                if (student.results.size() == 0) {
                    needed = false;
                } else if (student.results.get(0).risk().equals(SurveyResult.Risk.no)) {
                    needed = false;
                }
            }
            if (needed) {
                allCount++;
                if (student.interviewResults.size() == 0) {
                    noResultCount++;
                } else {
                    if (student.interviewResults.get(0).verdict.equals(InterviewResult.Verdict.refuted)) {
                        refutedCount++;
                    } else {
                        approvedCount++;
                    }
                }
            }
            if (type != null && type != 1) {
                renderArgs.put("type", type);
                if (type == 2) {
                    if (student.interviewResults.size() != 0) {
                        needed = false;
                    }
                } else if (type == 3) {
                    if (student.interviewResults.size() == 0) {
                        needed = false;
                    } else if (!student.interviewResults.get(0).verdict.equals(InterviewResult.Verdict.approved)) {
                        needed = false;
                    }
                } else if (type == 4) {
                    if (student.interviewResults.size() == 0) {
                        needed = false;
                    } else if (!student.interviewResults.get(0).verdict.equals(InterviewResult.Verdict.refuted)) {
                        needed = false;
                    }
                }
            }
            if (needed) {
                students.add(student);
            }
        }
        renderTemplate("SurveyController/interviewResults.html", students, allCount, refutedCount, noResultCount, approvedCount);
    }

    public static void newStudent() {
        renderArgs.put("back", true);
        renderArgs.put("backUrl", "/surveycontroller/index");
        renderArgs.put("error", flash.get("error"));
        render();
    }

    public static void surveyResultInterrupted() {
        render();
    }


    public static void deleteStudent(@Required Integer id) {
        if (Validation.hasErrors()) {
            index(null, null);
        }
        Account user = renderArgs.get("user", Account.class);
        Student student = Student.findById(id);
        if (student == null || !student.creator.id.equals(user.id)) {
            index(null, null);
        }
        student.deletedAt = new Date();
        student.save();
        index(null, null);
    }

    public static void saveStudent(@Required String number, @Required String gender /*, @Required String firstname, @Required String lastname, @Required String birthdate*/) {
        Account user = renderArgs.get("user", Account.class);
        if (Validation.hasErrors() || user.school == null || number.length() > 7 || !NumberUtils.isNumber(number)) {
            flash.put("error", "error");
            newStudent();
        }
        if (number.length() < 7) {
            int missingNumber = 7 - number.length();
            while (missingNumber > 0) {
                missingNumber--;
                number = "0" + number;
            }
        }
        Student student = new Student();
        //student.fullname = firstname + " " + lastname;
        student.school = user.school;
        student.creator = user;
        student.updatedAt = new Date();
        student.createdAt = new Date();
        student.gender = Gender.valueOf(gender);
        student.status = Student.Status.created;
        student.code = generateCode(number);
        student.offline = false;
        /*try {
            student.birthdate = new SimpleDateFormat("dd.MM.yyyy").parse(birthdate);
        } catch (ParseException e) {
            flash.put("error", "error");
            newStudent();
        }*/
        Student exixtingStudent = Student.find("code = ?1 and deletedAt is null", student.code).first();
        if (exixtingStudent != null) {
            flash.put("error", "error");
            newStudent();
        }
        try {
            student.save();
            flash.put("newStudent", student.dashedCode());
            index(null, null);
        } catch (PersistenceException e) {
            flash.put("error", "error");
            newStudent();
        }
    }

    public static void student(String code) {
        renderArgs.put("back", true);
        if (request.headers.containsKey("referer")) {
            String referrerUrl = request.headers.get("referer").values.get(0);
            if (referrerUrl.contains("surveycontroller/index") || referrerUrl.contains("surveycontroller/interviewresults") || referrerUrl.contains("leading/chat")) {
                renderArgs.put("backUrl", referrerUrl);
            } else {
                renderArgs.put("backUrl", "/surveycontroller/index");
            }
        } else {
            renderArgs.put("backUrl", "/surveycontroller/index");
        }
        if (code == null || code.trim().equals("")) {
            index(null, null);
        }
        code = code.replaceAll("-", "").trim();
        Student student = null;
        try {
            student = Student.find("code = ?1 and deletedAt is null", code).first();
        } catch (JPABase.JPAQueryException e) {
        }
        if (student != null) {
            Account account = renderArgs.get("user", Account.class);
            if (account.group.key.equals("psychologist")) {
                renderArgs.put("interviewWrite", true);
            } else {
                renderArgs.put("interviewWrite", false);
            }
            if (!hasPermissionsToGetInfo(account, student)) {
                index(null, null);
            }
            if (student.status.equals(Student.Status.finished)) {
                if (student.results != null && student.results.size() > 0) {
                    SurveyResult result = student.results.get(0);
                    String trimmedCode = code;
                    code = student.dashedCode();
                    result.allQuestionsJSON = null;
                    renderTemplate("SurveyController/surveyResult.html", result, code, trimmedCode);
                }
            } else if (student.status.equals(Student.Status.interrupted)) {
                if (student.stateAtInterruption != null && student.interruptedAt != null) {
                    try {
                        Integer interruptedAt = student.interruptedAt;
                        InterviewResult interview = InterviewResult.find("student = ?1", student).first();
                        String trimmedCode = code;
                        code = student.dashedCode();
                        renderTemplate("SurveyController/surveyResultInterrupted.html", student, code, interruptedAt, interview, trimmedCode);
                    } catch (JsonSyntaxException e) {

                    }
                }
            }
        }
        index(null, null);
    }

    private static boolean hasPermissionsToGetInfo(Account account, Student student) {
        if (account.id.equals(student.creator.id)) {
            return true;
        }
        StudentAndAccount studentAndAccount = StudentAndAccount.find("student = ?1 and account = ?2", student, account).first();
        if (studentAndAccount != null) {
            return true;
        }
        return false;
    }

    public static void downloadAnswers(String code) throws IOException, DocumentException {
        Student student = null;
        try {
            student = Student.find("code = ?1 and deletedAt is null", code).first();
        }
        catch (JPABase.JPAQueryException e) {
            Logger.error("Error occured while fetching student : " + e.getMessage(), e);
        }
        if (student != null) {
            Account account = renderArgs.get("user", Account.class);
            SurveyAnswersDto state = null;
            if (hasPermissionsToGetInfo(account, student)) {
                if (student.status == Student.Status.finished) {
                    if (student.results != null && !student.results.isEmpty()) {
                        SurveyResult result = student.results.get(0);
                        state = new Gson().fromJson(result.allQuestionsJSON, SurveyAnswersDto.class);
                    }
                }
                else {
                    if (student.stateAtInterruption != null && student.interruptedAt != null) {
                        state = new Gson().fromJson(student.stateAtInterruption, SurveyAnswersDto.class);
                    }
                }
            }
            else {
                Logger.debug("AdminUser " + account.id + " has no access to view result of the"
                        + "student " + student.id);
                forbidden();
            }
            if (state == null) {
                error();
            }
            else {
                Map<String, Object> params = new HashMap<String, Object>(3);
                params.put("state", state);

                StringBuilder fullHtml = new StringBuilder(1024);
                for (int i = 1; i < 14; i++) {
                    String path = "SurveyController/partials/interrupted/q" + i + ".html";
                    Template template = TemplateLoader.load(path);
                    String pageContents = template.render(params);
                    fullHtml.append(pageContents);
                    if (i != 13) {
                        fullHtml.append("<br/>");
                    }
                }
                response.setHeader("Content-Type", "application/pdf");
                response.setHeader("Content-Disposition", "inline;filename=\"" + code + ".pdf\"");
                renderBinary(PdfCreator.fromAnswersHtml(code, fullHtml.toString()));
            }
        }
        else {
            Logger.debug("Student with code was not found : " + code);
            notFound();
        }
    }

    public static void saveInterview(String text, @Required Integer evaluation, @Required String code) {
        if (Validation.hasErrors()) {
            renderText("error");
        }
        Account user = renderArgs.get("user", Account.class);
        Student student = Student.find("code = ?1 and deletedAt is null", code).first();
        if (student == null || !user.id.equals(student.creator.id) || student.status.equals(Student.Status.created) || (!student.status.equals(Student.Status.interrupted) && student.results.get(0).risk().equals(SurveyResult.Risk.no))) {
            renderText("error");
        }
        InterviewResult result = InterviewResult.find("student = ?1", student).first();
        if (result == null) {
            result = new InterviewResult();
        }
        result.text = text;
        if (student.status.equals(Student.Status.interrupted)) {
            result.risk = SurveyResult.Risk.high;
        } else {
            result.risk = student.results.get(0).risk();
        }
        result.updatedAt = new Date();
        if (evaluation == 0) {
            result.verdict = InterviewResult.Verdict.refuted;
        } else {
            result.verdict = InterviewResult.Verdict.approved;
        }
        result.student = student;
        result.save();
        if (!student.status.equals(Student.Status.interrupted)) {
            SurveyResult surveyResult = student.results.get(0);
            surveyResult.interviewResult = result;
            surveyResult.save();
        }
        renderText("success");
    }

    public static void survey() {
        renderArgs.put("headerContent", true);
        render();
    }

    public static void surveyQuestions(String code, String psychoName) {
        Student student = null;
        if (psychoName != null) {
            psychoName = psychoName.trim();
        }
        if (code != null) {
            code = code.replaceAll("-", "").trim();
        }

        Account parent = null;
        try {
            parent = JPA.em().createQuery("Select a from Account a where username = ?1", Account.class)
                    .setParameter(1, psychoName)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (JPABase.JPAQueryException e) {
        } catch (NoResultException e) {
            Logger.info("account is not found with  given username");
        }

        try {
            student = Student.find("code = ?1 and creator = ?2 and deletedAt is null", code, parent).first();
        } catch (JPABase.JPAQueryException e) {
        }

        if (student != null) {
            if (student.status.equals(Student.Status.created)) {
                renderTemplate("SurveyController/partials/questions.html");
            }
        }
        renderText("error");
    }

    public static void interruptSurvey(String code, String login, Integer interruptedAt, String state) {
        if (code != null) {
            code = code.replaceAll("-", "").trim();
        } else {
            renderText("error");
        }

        if (login != null) {
            login = login.trim();
        } else {
            renderText("error");
        }

        Account parent = null;
        try {
            parent = JPA.em().createQuery("Select a from Account a where username = ?1", Account.class)
                    .setParameter(1, login)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (JPABase.JPAQueryException e) {
        } catch (NoResultException e) {
            Logger.info("account is not found with  given username");
        }

        Student student = Student.find("code = ?1 and creator = ?2 and deletedAt is null", code, parent).first();
        if (student != null) {
            student.status = Student.Status.interrupted;
            student.interruptedAt = interruptedAt;
            student.stateAtInterruption = state;
            student.updatedAt = new Date();
            student.save();
            renderText("success");
        }
        renderText("error");
    }

    public static void submitResults() {
        StringWriter writer = new StringWriter();
        try {
            IOUtils.copy(request.body, writer, "UTF-8");
        } catch (IOException e) {
            renderText("error 1");
        }
        SurveyAnswersDto dto = null;
        String json = writer.toString();
        try {
            dto = new Gson().fromJson(json, SurveyAnswersDto.class);
        } catch (JsonSyntaxException e) {
            renderText("error 2");
        }
        if (dto == null || dto.code == null) {
            renderText("error 3");
        }
        if (dto.code != null) {
            dto.code = dto.code.replaceAll("-", "").trim();
        }
        if (dto.login != null) {
            dto.login = dto.login.trim();
        }

        Account parent = null;
        try {
            parent = JPA.em().createQuery("Select a from Account a where username = ?1", Account.class)
                    .setParameter(1, dto.login)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (JPABase.JPAQueryException e) {
        } catch (NoResultException e) {
            Logger.info("account is not found with  given username");
        }


        Student student = Student.find("code = ?1 and creator = ?2 and deletedAt is null", dto.code, parent).first();
        if (student == null) {
            renderText("error 4");
        }
        try {
            SurveyResult result = SurveyUtils.analyzeResults(dto);
            result.allQuestionsJSON = json;
            result.student = student;
            result.createdAt = new Date();
            result.save();
        } catch (Exception e) {
            renderText("error 5");
        }
        student.status = Student.Status.finished;
        student.updatedAt = new Date();
        student.save();
        renderText("success");
    }

    private static String generateCode(String code) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        code = code + calendar.get(Calendar.YEAR) % 100;
        String first = encrpyt(code.substring(0, 3));
        String second = encrpyt(code.substring(3, 6));
        String third = encrpyt(code.substring(6, 9));
        return third + second + first;
    }

    private static String encrpyt(String input) {
        Integer integerValue = Integer.valueOf(input);
        String result = bigPower(integerValue, E).mod(BigInteger.valueOf(N)).toString();
        if (result.length() < 4) {
            int missingNumber = 4 - result.length();
            while (missingNumber > 0) {
                missingNumber--;
                result = "0" + result;
            }
        }
        return result;
    }

    private static BigInteger bigPower(int number, int n) {
        BigInteger result = new BigInteger("1");
        for (int i = 0; i < n; i++) {
            result = result.multiply(BigInteger.valueOf(number));
        }
        return result;
    }
}
