package controllers;

import com.google.gson.*;
import models.*;
import play.Logger;
import play.db.jpa.JPA;
import play.mvc.Controller;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tauyekel on 6/7/17.
 */
public class Ajax extends Controller {

    private static class AccountSerializable {
        String username;
        String generatedPassword;

        public AccountSerializable(String username, String generatedPassword) {
            this.username = username;
            this.generatedPassword = generatedPassword;
        }
    }

    private static class GroupWithAccounts {
        String name;
        Integer generatedBatch;
        List<AccountSerializable> accounts;

        public GroupWithAccounts(String name, List<AccountSerializable> accounts, Integer generatedBatch) {
            this.name = name;
            this.accounts = accounts;
            this.generatedBatch = generatedBatch;
        }
    }

    public static void steps(Integer groupId) {
        Group group = Group.findById(groupId);
        List<Step> steps = Step.find("group = ?1", group).fetch();
        for (Step step: steps) {
            step.group = null;
            step.lessons = null;
        }
        renderJSON(steps);
    }

    public static void provinces(Integer regionId) {
        Location region = Location.findById(regionId);
        List<Location> provinces = Location.find("parent0 = ?1", region).fetch();
        renderJSON(provinces);
    }

    public static void cities(Integer provinceId) {
        Location province = Location.findById(provinceId);
        List<Location> cities = Location.find("parent0 = ?1", province).fetch();
        renderJSON(cities);
    }

    public static void schools() {
        List<School> schools = new ArrayList<School>();

        if (params.get("city_id") != null && !params.get("city_id").equals("")) {
            Integer cityId = Integer.parseInt(params.get("city_id"));
            Location city = Location.findById(cityId);
            schools = School.find("city = ?1", city).fetch();
        } else {
            if (params.get("province_id") != null && !params.get("province_id").equals("") && schools.size() < 1) {
                Integer provinceId = Integer.parseInt(params.get("province_id"));
                Location province = Location.findById(provinceId);
                schools = School.find("province = ?1", province).fetch();
            } else {
                if (params.get("region_id") != null && !params.get("region_id").equals("") && schools.size() < 1) {
                    Integer regionId = Integer.parseInt(params.get("region_id"));
                    Location region = Location.findById(regionId);
                    schools = School.find("region = ?1", region).fetch();
                }
            }
        }

        if (schools.size() < 1) {
            schools = School.findAll();
        }
        renderJSON(schools);
    }

    public static void accounts() throws UnsupportedEncodingException {
        Integer schoolId = Integer.parseInt(params.get("schoolId"));
        School school = School.findById(schoolId);
        Integer maxBatch = JPA.em().createQuery("Select max(a.generatedBatch) from Account a", Integer.class).getSingleResult();
        Integer newMaxBatch = maxBatch + 1;

        JsonObject counts = new JsonParser().parse(params.get("counts")).getAsJsonObject();
        List<Group> groups = Group.findAll();
        List<GroupWithAccounts> groupsWithAccounts = new ArrayList<GroupWithAccounts>();

        for (Group group : groups) {
            if (counts.has(group.id.toString())) {
                Integer accountCountPerGroup = counts.get(group.id.toString()).getAsInt();
                List<AccountSerializable> accounts = new ArrayList<AccountSerializable>();

                for (int i = 0; i < accountCountPerGroup; i++) {
                    Account account = new Account(group.key, null, group, school, newMaxBatch);
                    account.save();
                    account.username = account.username + account.id;
                    account.save();
                    AccountSerializable accountSerializable = new AccountSerializable(account.username, account.generatedPassword);
                    accounts.add(accountSerializable);
                }

                GroupWithAccounts groupWithAccounts = new GroupWithAccounts(group.name, accounts, newMaxBatch);
                groupsWithAccounts.add(groupWithAccounts);
            }
        }
        renderJSON(groupsWithAccounts);
    }
}
