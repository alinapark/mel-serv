package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.List;

/**
 * Created by tauyekel on 6/19/17.
 */

@Entity
@Table(name="news_types")
public class NewsType extends GenericModel {

    public NewsType(String name, String key) {
        this.name = name;
        this.key = key;
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="name_kz")
    public String nameKz;

    @Column(name="name_en")
    public String nameEn;

    @Column(name="key")
    public String key;

    @OneToMany(mappedBy = "newsType", fetch = FetchType.LAZY)
    public List<News> news;
}
