package models;

import play.db.jpa.GenericModel;
import play.db.jpa.JPA;
import utils.BCrypt;
import utils.RandomPassword;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by sagynysh on 5/17/17.
 */
@Entity
@Table(name="accounts")
public class Account extends GenericModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="username")
    public String username;

    @Column(name="password")
    public String password;

    @Column(name="generated_password")
    public String generatedPassword;

    @Column(name="generated_batch")
    public Integer generatedBatch;

    @Column(name="is_finish_course", columnDefinition = "bit", nullable = false)
    public Boolean isCourseFinished;

    @Column(name="is_finish_quiz_first", columnDefinition = "bit", nullable = false)
    public Boolean isFirstQuizFinished;

    @Column(name="is_finish_quiz_last", columnDefinition = "bit", nullable = false)
    public Boolean isLastQuizFinished;

    @ManyToOne
    @JoinColumn(name="group_id")
    public Group group;

    @ManyToOne
    @JoinColumn(name="school_id")
    public School school;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY)
    public List<UserQuestion> questions;

    @Transient
    public Integer getCurrentPage() {
        try {
            return JPA.em().createQuery("Select q from QuizScore q where account = ?1 and q.isPassed = true order by q.quiz.step.position desc", QuizScore.class)
                    .setParameter(1, this)
                    .setMaxResults(1)
                    .getSingleResult()
                    .quiz.step.position;
        } catch (NoResultException e) {
            return 0;
        }
    }

    @Transient
    public Profile getProfile() {
        try {
            return Profile.find("account = ?1", this).first();
        } catch (Exception e) {
            return null;
        }
    }

    public Account(String username, String password, Group group, School school, Integer generatedBatch) {
        this.username = username;
        if (password == null) {
            String genPassword = RandomPassword.genRandomPassword(6);
            this.generatedPassword = genPassword;
            this.password = BCrypt.hashpw(genPassword, BCrypt.gensalt());
        } else {
            this.password = password;
            this.generatedPassword = "generated";
        }
        this.isCourseFinished = false;
        this.isFirstQuizFinished = false;
        this.isLastQuizFinished = false;
        this.group = group;
        this.school = school;
        this.generatedBatch = generatedBatch;
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }
}
