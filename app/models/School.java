package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by tauyekel on 5/31/17.
 */
@Entity
@Table(name="schools")
public class School extends GenericModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @ManyToOne
    @JoinColumn(name="country_id")
    public Location country;

    @ManyToOne
    @JoinColumn(name="region_id")
    public Location region;

    @ManyToOne
    @JoinColumn(name="province_id")
    public Location province;

    @ManyToOne
    @JoinColumn(name="city_id")
    public Location city;
}
