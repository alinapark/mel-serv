package models;

import enums.Gender;
import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by sagynysh on 7/5/17.
 */
@Entity
@Table(name="students")
public class Student extends GenericModel {

    public enum Status {
        created, finished, interrupted
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

/*    @Column(name="fullname")
    public String fullname;*/

    @Column(name="code")
    public String code;

    @ManyToOne
    @JoinColumn(name="creator_id")
    public Account creator;

    @ManyToOne
    @JoinColumn(name="school_id")
    public School school;

/*    @Column(name="birthdate")
    public Date birthdate;*/

    @Enumerated(EnumType.STRING)
    @Column(name="gender", columnDefinition = "ENUM('m', 'f')")
    public Gender gender;

    @OneToMany(mappedBy = "student", fetch = FetchType.EAGER)
    public List<SurveyResult> results;

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
    public List<InterviewResult> interviewResults;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @Column(name="deleted_at")
    public Date deletedAt;

    @Enumerated(EnumType.STRING)
    @Column(name="status", columnDefinition = "ENUM('created', 'finished', 'interrupted')")
    public Status status;

    @Column(name="interrupted_at")
    public Integer interruptedAt;

    @Column(name="state_at_interruption", columnDefinition = "TEXT")
    public String stateAtInterruption;

    @Column(name="is_offline", columnDefinition = "BIT", length = 1)
    public Boolean offline;

    @Transient
    public String dashedCode() {
        StringBuilder str = new StringBuilder(this.code);
        int idx = str.length() - 4;
        while (idx > 0) {
            str.insert(idx, "-");
            idx = idx - 4;
        }
        return str.toString();
    }

    @Transient
    public String updatedDate() {
        if (updatedAt == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(updatedAt);
    }

    @Transient
    public static Student findByCode(String code) {
        if (code == null || code.trim().equals("")) {
            return null;
        }
        code = code.trim().replaceAll("-", "");
        Student student = Student.find("code = ?1 and deletedAt is null", code).first();
        return student;
    }
}
