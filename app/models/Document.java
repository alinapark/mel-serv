package models;

import enums.Language;
import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by tauyekel on 5/19/17.
 */
@Entity
@Table(name="documents")
public class Document extends GenericModel {

    public Document(String name, String file, Language language, Group group, Step step) {
        this.name = name;
        this.file = file;
        this.language = language;
        this.group = group;
        this.step = step;
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="name")
    public String name;

    @Column(name="file")
    public String file;

    @Enumerated(EnumType.STRING)
    @Column(name="language", columnDefinition = "ENUM('ru', 'kz')")
    public Language language;

    @ManyToOne
    @JoinColumn(name="group_id")
    public Group group;

    @ManyToOne
    @JoinColumn(name="step_id")
    public Step step;
}
