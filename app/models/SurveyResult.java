package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by tauyekel on 7/24/17.
 */

@Entity
@Table(name="survey_results")
public class SurveyResult extends GenericModel {

    public enum Risk {
        high, presents, low, no
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="suicide_thoughts_scale")
    public Integer suicideThoughtsScale;

    //ДЕПРЕССИЯ ТРЕВОГА И СТРЕСС
    @Column(name="depression_scale")
    public Integer depressionScale;

    @Column(name="anxiety_scale")
    public Integer anxietyScale;

    @Column(name="stress_scale")
    public Integer stressScale;

    //ЭМОЦИОНАЛЬНОЕ СОСТОЯНИЕ
    @Column(name="emotional_symptoms_scale")
    public Integer emotionalSymptomsScale;

    @Column(name="behavioral_problems_scale")
    public Integer behavioralProblemsScale;

    @Column(name="hyperactivity_scale")
    public Integer hyperactivityScale;

    @Column(name="peer_problems_scale")
    public Integer peerProblemsScale;

    @Column(name="prosocial_behavior_scale")
    public Integer prosocialBehaviorScale;

    @Column(name="total_problems_scale")
    public Integer totalProblemsScale;

    //НЕОТЛОЖНЫЙ СЛУЧАЙ
    @Column(name="is_suicide_heavy", columnDefinition = "BIT")
    public Boolean isSuicideHeavy;

    //В ГРУППЕ РИСКА
    @Column(name="is_in_risk_group", columnDefinition = "BIT")
    public Boolean isInRiskGroup;

    //СКЛОНЕН К САМОПОВРЕЖДЕНИЯМ
    @Column(name="is_self_damaging", columnDefinition = "BIT")
    public Boolean isSelfDamaging;

    //ПОЛЕЗНАЯ ИНФОРМАЦИЯ СЛАЙД №9
    @Column(name="suicide_method", columnDefinition = "TEXT")
    public String suicideMethod;

    @Column(name="has_got_medical_help", columnDefinition = "BIT")
    public Boolean hasGotMedicalHelp;

    @Column(name="has_not_talked_to_someone", columnDefinition = "BIT")
    public Boolean hasNotTalkedToSomeone;

    //ПОПЫТКА СУИЦИДА
    @Column(name="has_tried_to_commit_suicide", columnDefinition = "BIT")
    public Boolean hasTriedToCommitSuicide;

    @ManyToOne
    @JoinColumn(name="student_id")
    public Student student;

    //ОБЩАЯ ИНФОРМАЦИЯ, НЕ ИЗ СЛАЙДОВ
    @Column(name="q1")
    public Integer q1;

    @Column(name="q2")
    public Integer q2;

    @Column(name="q3")
    public Integer q3;

    //ВЕСЬ РЕКВЕСТ
    @Column(name="all_questions_json", columnDefinition = "TEXT")
    public String allQuestionsJSON;

    @OneToOne
    @JoinColumn(name = "interview_result")
    public InterviewResult interviewResult;

    @Column(name="created_at")
    public Date createdAt;

    @Enumerated(EnumType.STRING)
    @Column(name="risk", columnDefinition = "ENUM('high', 'presents', 'low', 'no')")
    public Risk risk;

    @Transient
    public Risk risk() {
        if (isSuicideHeavy) {
            return Risk.high;
        }
        if (isInRiskGroup) {
            return Risk.presents;
        }
        if (prosocialBehaviorScale <= 4) {
            return Risk.low;
        }
        return Risk.no;
    }

    @Transient
    public String formattedDate() {
        if (createdAt == null) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        return format.format(createdAt);
    }
}
