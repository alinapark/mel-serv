package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by tauyekel on 5/24/17.
 */
@Entity
@Table(name="quiz_questions")
public class QuizQuestion extends GenericModel {

    public QuizQuestion(Quiz quiz, String value, String description, Integer position) {
        if (value != null) {
            value = value.trim();
        }
        if (description != null) {
            description = description.trim();
        }
        this.quiz = quiz;
        this.value = value;
        this.description = description;
        this.position = position;
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    @JoinColumn(name="quiz_id")
    public Quiz quiz;

    @Column(name="value")
    public String value;

    @Column(name="description", columnDefinition = "TEXT")
    public String description;

    @Column(name="position")
    public Integer position;

    @Column(name="created_at")
    public Date createdAt;

    @Column(name="updated_at")
    public Date updatedAt;

    @Column(name="deleted_at")
    public Date deletedAt;

    public QuizResult getQuestionResult(QuizScore quizScore) {
        return QuizResult.find("score = ?1 and question = ?2", quizScore, this).first();
    }
}
