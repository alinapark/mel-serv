package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by sagynysh on 8/16/17.
 */
@Entity
@Table(name="supervisors")
public class Supervisor extends GenericModel {

    public enum Type {
        republic, region, province
    }

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name="login_", unique = true)
    public String login;

    @Column(name="fullname")
    public String fullname;

    @Column(name="password")
    public String password;

    @ManyToOne
    @JoinColumn(name="region_id")
    public Location region;

    @ManyToOne
    @JoinColumn(name="province_id")
    public Location province;

    @Enumerated(EnumType.STRING)
    @Column(name="type_", columnDefinition = "ENUM('republic', 'region', 'province')")
    public Type type;

}
