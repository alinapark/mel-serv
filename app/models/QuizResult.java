package models;

import play.db.jpa.GenericModel;

import javax.persistence.*;

/**
 * Created by tauyekel on 5/24/17.
 */
@Entity
@Table(name="quiz_results")
public class QuizResult extends GenericModel {

    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @ManyToOne
    @JoinColumn(name="score_id")
    public QuizScore score;

    @ManyToOne
    @JoinColumn(name="question_id")
    public QuizQuestion question;

    @ManyToOne
    @JoinColumn(name="answer_id")
    public QuizAnswer answer;

    @Column(name="is_right", columnDefinition = "BIT", length = 1)
    public Boolean isRight;
}
