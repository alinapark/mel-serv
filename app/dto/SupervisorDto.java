package dto;

import models.Supervisor;

public class SupervisorDto {

    public Supervisor.Type type;
    public String regionName;
    public int regionId;
    public String provinceName;
    public int provinceId;

    public SupervisorDto(Supervisor supervisor) {
        this.type = supervisor.type;
        if (!this.type.equals(Supervisor.Type.republic)) {
            this.regionName = supervisor.region.name;
            this.regionId = supervisor.region.id;
            if (!this.type.equals(Supervisor.Type.region)) {
                this.provinceId = supervisor.province.id;
                this.provinceName = supervisor.province.name;
            }
        }
    }
}
