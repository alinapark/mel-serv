package utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Documentation for {@code StudentLeadingUtils}.
 *
 * @author Timur Tibeyev.
 */
public class StudentLeadingUtils {

    /**
     * Converts string representation of pages to list of page numbers.
     *
     * @param str string representing pages ex "1,2-4"
     * @return list of page numbers
     */
    public static Set<Integer> parsePagesString(String str) {
        Set<Integer> result = new HashSet<>();
        String splitted[] = str.split(",");
        for (String el : splitted) {
            String noSpace = el.trim().replace(" ", "");
            if (noSpace.contains("-")) {
                String[] period = noSpace.split("-");
                int start = Integer.parseInt(period[0]);
                int finish = Integer.parseInt(period[1]);
                for (int i = start; i <= finish; i++) {
                    result.add(i);
                }
            }
            else {
                if (!noSpace.isEmpty()) {
                    int page = Integer.parseInt(noSpace);
                    result.add(page);
                }
            }
        }
        return result;
    }
}
