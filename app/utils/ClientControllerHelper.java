package utils;

import play.Logger;
import play.Play;

import java.io.*;
import java.util.Date;

public class ClientControllerHelper {

    /**
     * Receives file and stores it inside news resources folder.
     *
     * @param file the student code
     * @return name of the stored file
     */
    public static String storeNewsResource(File file) {
        final String fileDirectory = Play.configuration.getProperty("fileDirectory");
        final String newsPath = "/storage/news/";
        String result = null;
        if (file != null) {
            File destFile;

            String old = file.getName();
            int lastDot = old.lastIndexOf('.');
            String ext = old.substring(lastDot, old.length());

            result = new Date().getTime() + ext;
            destFile = new File(fileDirectory + newsPath + result);
            destFile.getParentFile().mkdirs();

            try {
                destFile.createNewFile();
                byte[] bytes = ClientControllerHelper.readBytesFromFile(file);
                ClientControllerHelper.writeBytesToFile(destFile, bytes);
            } catch (IOException e) {
                Logger.error(e,"Error during saving file");
            }
        }
        return result;
    }

    public static byte[] readBytesFromFile(File file) throws IOException {
        InputStream inputStream = new FileInputStream(file);
        long length = file.length();
        if(length > Integer.MAX_VALUE) {
            throw new IOException("Could not completely read file " +
                    file.getName() + " as it is too long (" + length + " bytes, max supported " +
                    Integer.MAX_VALUE + ")");
        }

        byte[] bytes = new byte[(int)length];
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length && (numRead = inputStream.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if(offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        inputStream.close();
        return bytes;
    }

    public static void writeBytesToFile(File theFile, byte[] bytes) throws IOException {
        BufferedOutputStream bufferedOutputStream = null;
        try {
            FileOutputStream fos = new FileOutputStream(theFile);
            bufferedOutputStream = new BufferedOutputStream(fos);
            bufferedOutputStream.write(bytes);
        } finally {
            if(bufferedOutputStream != null) {
                try {
                    bufferedOutputStream.flush();
                    bufferedOutputStream.close();
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}