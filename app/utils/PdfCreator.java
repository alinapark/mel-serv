package utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import play.Play;

/**
 * Class responible for working with PDF files.
 *
 * @author Timur Tibeyev.
 */
public class PdfCreator {

    /** The css file. */
    private static final File CSS_FILE = Play.getFile("public/css/app.css");
    /** The folder containing fonts. */
    private static final File FONTS_FOLDER = Play.getFile("public/css/fonts");

    /**
     * Parses html string, generates PDF file and returns result file.
     *
     * @param code the student code
     * @param innerHtml the html string
     * @return generated PDF file
     */
    public static File fromAnswersHtml(String code, String innerHtml) throws IOException, DocumentException {

        final String fileName = System.currentTimeMillis() + "";
        File file = File.createTempFile(fileName,".pdf");
        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();

        XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(
                XMLWorkerFontProvider.DONTLOOKFORFONTS);
        fontProvider.register(new File(FONTS_FOLDER, "Roboto_Light.ttf").getPath(),
                "Roboto");

        String html = "<div class=\"block\" style=\"background-color: white; margin-left: 1rem; "
                + "margin-right: 1rem\">"
                + "<div class=\"text-group\" style=\"font-family: roboto;\">"
                + "<h1>"
                + code
                + "</h1>"
                + innerHtml
                + "</div></div>";

        XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
        InputStream his = new ByteArrayInputStream(html.getBytes("UTF-8"));
        InputStream cis = new FileInputStream(CSS_FILE);
        worker.parseXHtml(pdfWriter, document, his, cis, Charset.forName("UTF-8"), fontProvider);
        document.close();

        return file;
    }

    /**
     * Parses html string, generates PDF file and returns result file.
     *
     * @param code the student code
     * @param innerHtml the html string
     * @return generated PDF file
     */
    public static File fromStudentLeadingHtml(String code, String innerHtml) throws IOException, DocumentException {

        final String fileName = System.currentTimeMillis() + "";
        File file = File.createTempFile(fileName,".pdf");
        Document document = new Document();
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(file));
        document.open();

        XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(
                XMLWorkerFontProvider.DONTLOOKFORFONTS);
        fontProvider.register(new File(FONTS_FOLDER, "Roboto_Light.ttf").getPath(),
                "Roboto");

        String html = "<div class=\"block\" style=\"background-color: white; margin-left: 1rem; "
                + "margin-right: 1rem\">"
                + "<div class=\"text-group\" style=\"font-family: roboto;\">"
                + "<h1>"
                + code
                + "</h1>"
                + innerHtml
                + "</div></div>";

        XMLWorkerHelper worker = XMLWorkerHelper.getInstance();
        InputStream his = new ByteArrayInputStream(html.getBytes("UTF-8"));
        InputStream cis = new FileInputStream(CSS_FILE);
        worker.parseXHtml(pdfWriter, document, his, cis, Charset.forName("UTF-8"), fontProvider);
        document.close();

        return file;
    }
}
