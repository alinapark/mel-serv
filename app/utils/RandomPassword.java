package utils;

import java.util.Random;

/**
 * Created by tauyekel on 6/1/17.
 */
public class RandomPassword {

    public static String genRandomPassword(int length) {
        char[] chars = "1234567890".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }
}
