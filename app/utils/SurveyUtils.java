package utils;

import dto.SurveyAnswersDto;
import models.SurveyResult;

public class SurveyUtils {

    public static SurveyResult analyzeResults(SurveyAnswersDto answer) throws Exception {
        SurveyResult result = new SurveyResult();

        int suicideThoughts = 0;
        int depression = 0;
        int anxiety = 0;
        int stress = 0;
        int emotionalSymptoms = 0;
        int behavioralProblems = 0;
        int hyperactivity = 0;
        int peerProblems = 0;
        int prosocialBehavior = 0;
        int q13 = 0;
        int totalProblems = 0;

        boolean hasTriedToCommitSuicide = false;
        boolean isSuicideHeavy = false;
        boolean isInRiskGroup = false;
        boolean isSelfDamaging = false;

        for (int i = 0; i < answer.q5.size(); i++) {
            suicideThoughts += answer.q5.get(i);
        }

        if (answer.q5.get(3) >= 2 || answer.q8 == 1) {
            isSuicideHeavy = true;
            isInRiskGroup = true;
        }

        //calculating scores
        //depression, anxiety, stress
        for (int i = 0; i < answer.q4.size(); i++) {
            if (i == 2 ||
                    i == 4 ||
                    i == 9 ||
                    i == 12 ||
                    i == 15 ||
                    i == 16 ||
                    i == 20) {
                depression += answer.q4.get(i);
            }

            if (i == 1 ||
                    i == 3 ||
                    i == 6 ||
                    i == 8 ||
                    i == 14 ||
                    i == 18 ||
                    i == 19) {
                anxiety += answer.q4.get(i);
            }

            if (i == 0 ||
                    i == 5 ||
                    i == 7 ||
                    i == 10 ||
                    i == 11 ||
                    i == 13 ||
                    i == 17) {
                stress += answer.q4.get(i);
            }
        }

        //emotional symptoms, behavioral problems, hyperactivity, peer problems, prosocial behavior
        for (int i = 0; i < answer.q12.size(); i++) {
            if (i == 3 ||
                    i == 7 ||
                    i == 12 ||
                    i == 15 ||
                    i == 23) {
                emotionalSymptoms += answer.q12.get(i);
            }

            if (i == 4 ||
                    i == 6 ||
                    i == 11 ||
                    i == 17 ||
                    i == 21) {
                behavioralProblems += answer.q12.get(i);
            }

            if (i == 1 ||
                    i == 9 ||
                    i == 14||
                    i == 20 ||
                    i == 24) {
                hyperactivity += answer.q12.get(i);
            }

            if (i == 5 ||
                    i == 10 ||
                    i == 13||
                    i == 18 ||
                    i == 22) {
                peerProblems += answer.q12.get(i);
            }

            if (i == 0 ||
                    i == 2 ||
                    i == 8 ||
                    i == 16 ||
                    i == 19) {
                prosocialBehavior += answer.q12.get(i);
            }

        }
        //total problems
        totalProblems += emotionalSymptoms + behavioralProblems + hyperactivity + peerProblems;

        //is in self-damaging group?
        for (Integer q13sub: answer.q13) {
            q13 += q13sub;
        }
        if (q13 >= 3) {
            isSelfDamaging = true;
            isInRiskGroup = true;
        }

        if (totalProblems >= 20 || stress >= 13 || anxiety >= 8 || depression >= 11 || suicideThoughts >= 2) {
            isInRiskGroup = true;
        }

        if (answer.q6 == 1) {
            hasTriedToCommitSuicide = true;
            isInRiskGroup = true;
            result.suicideMethod = answer.q9;
            result.hasGotMedicalHelp = answer.q10 == 1;
            boolean noone = false;
            for (Integer i: answer.q11.array) {
                if (i == 5) {
                    noone = true;
                }
            }
            result.hasNotTalkedToSomeone = noone;
        }

        //saving in model
        //scales
        result.depressionScale = depression;
        result.anxietyScale = anxiety;
        result.stressScale = stress;
        result.emotionalSymptomsScale = emotionalSymptoms;
        result.behavioralProblemsScale = behavioralProblems;
        result.hyperactivityScale = hyperactivity;
        result.peerProblemsScale = peerProblems;
        result.prosocialBehaviorScale = prosocialBehavior;
        result.totalProblemsScale = totalProblems;
        result.hasTriedToCommitSuicide = hasTriedToCommitSuicide;
        result.isInRiskGroup = isInRiskGroup;
        result.isSuicideHeavy = isSuicideHeavy;
        result.isSelfDamaging = isSelfDamaging;
        result.suicideThoughtsScale = suicideThoughts;

        //general information
        result.q1 = answer.q1;
        result.q2 = answer.q2;
        result.q3 = answer.q3;

        result.risk = result.risk();
        return result;
    }
}
