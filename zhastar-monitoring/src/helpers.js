export function getURLwithoutRoot(string) {
   let nthPosition = string.split('/', 2).join('/').length;
   return string.substring(nthPosition);
}

export function deleteCookie(name) {
  document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}