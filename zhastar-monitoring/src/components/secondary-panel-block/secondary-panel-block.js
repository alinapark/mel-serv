import React from 'react';
import PropTypes from "prop-types";

import "./secondary-panel-block.css";

import SecondaryPanelBlockRow from "./__row/secondary-panel-block__row";

const SecondaryPanelBlock = ({secondaryPanelBlockArray}) => (
    <div className="secondary-panel-block">
    	{ secondaryPanelBlockArray.map(row => <SecondaryPanelBlockRow key={secondaryPanelBlockArray.indexOf(row)} row={row}/>) }
    </div>
)

SecondaryPanelBlock.propTypes = {
    secondaryPanelBlockArray: PropTypes.arrayOf(PropTypes.array).isRequired
}

export default SecondaryPanelBlock;
