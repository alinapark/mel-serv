import React from 'react';
import './panel-content.css';
import PropTypes from 'prop-types';
import { Doughnut } from 'react-chartjs-2';

import PanelContentGraph from "./panel-content__graph/panel-content__graph";

const PanelContent = ({ graph1Data, graph2Data, graph3Data, panelContentClassNames, panelContentGraphClassNames, isMain }) => (
    <div className={`panel-content ${panelContentClassNames !== undefined ? panelContentClassNames : ""}`}>
        <PanelContentGraph 
            graphData={graph1Data}
            panelContentGraphClassNames={panelContentGraphClassNames}
            isMain={isMain}
        />
        <PanelContentGraph 
            graphData={graph2Data}
            panelContentGraphClassNames={panelContentGraphClassNames}
            isMain={isMain}
        />
        <PanelContentGraph 
            graphData={graph3Data}
            panelContentGraphClassNames={panelContentGraphClassNames}
            isMain={isMain}
        />
    </div>
)

PanelContent.propTypes = {
	panelContentClassNames: PropTypes.arrayOf(PropTypes.string),
}


export default PanelContent;
