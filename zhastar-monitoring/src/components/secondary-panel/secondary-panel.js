import React  from 'react';
import PropTypes from "prop-types";

import "./secondary-panel.css";
import PanelContent from  "../panel-content/panel-content";

const SecondaryPanel = ({ title, secondary1, secondary2, secondary3, onClick, clickable }) =>  (
	<div className={`card secondary-panel ${clickable ? "secondary-panel_hoverable" : ""}`} onClick={clickable && onClick}>
	    <p className="card-header-title" id={`${title === 'Акмолинская область валдповлап вадплодалвпо вадлпоадлпро вадлпоадлпро вадлподалповр ыдпловдлпо' ? "test-title" : ""}`}>
	        { title }
	    </p>
	    <PanelContent 
	    	panelContentClassNames={["secondary-panel__panel-content"]}
	    	panelContentGraphClassNames={["panel-content__graph_small"]} 
	    	graph1Data={secondary1}
	    	graph2Data={secondary2}
	    	graph3Data={secondary3}
	    	isMain={false}
	    />
	</div>
)

SecondaryPanel.propTypes = {
	title: PropTypes.string.isRequired,
	secondary1: PropTypes.object.isRequired,
	secondary2: PropTypes.object.isRequired,
	secondary3: PropTypes.object.isRequired,
	clickable: PropTypes.bool.isRequired,
	onClick: PropTypes.func
}

export default SecondaryPanel;
