import React, { Component } from 'react';

import './z_css/Monitoring.css';

import MainPanel from "./main-panel/main-panel";
import ControlPanel from "./control-panel/control-panel";
import SecondaryPanelBlock from "./secondary-panel-block/secondary-panel-block";
import LoaderCustom from "./loader-custom/loader-custom";
import Header from "./header/header";

import { getURLwithoutRoot, deleteCookie } from "../helpers";
import { isBackRender, fetchData, selectMainPanel, selectSecondaryPanelBlock, validate } from "./MonitoringCalculations";

//js plugins
import cardTitleShortenLong from "../plugins/cardTitleShortenLong";
import centerDoughnutText from "../plugins/centerDoughnutText";


class Monitoring extends Component {
	constructor() {
		super();
		this.state = {
			render: false,
			data: "",
			filterNameInput: "",
			filterSortDropdown: "",
			isBackRender: false
		}
	}



	componentWillMount() {
		fetchData(this.props, this.setState.bind(this));
		validate(this.setState.bind(this));
	}

	componentDidMount() {
		cardTitleShortenLong();
		centerDoughnutText();
	}

	componentDidUpdate() {
		//shorten long titles on each update
		cardTitleShortenLong();
		centerDoughnutText();
	}

	

	componentWillReceiveProps(nextProps) {
		isBackRender(nextProps, this.state, this.setState.bind(this));

		if (this.props.location.pathname !== nextProps.location.pathname) {
			this.setState({
				render1: false
			});

			fetchData(nextProps, this.setState.bind(this));
		}
	}

	

	handlePanelClick = (nextScope, key) => {
		this.props.history.push(`/monitoring/${nextScope}/${key}`);
	}


	handleLogout = () => {
		fetch("/monitoring/api/logout", {
			credentials: "same-origin"
		})
			.then((res) => {
				if (res.status !== 200 || !res.ok) {
					throw new Error("Проблемы с соединением");
				}

				return res.text()
			})
			.then((text) => {
				this.props.history.push('/monitoring');
			})
			.catch((e) => {
				alert("Ошибка: " + e.message);
			})
	}
	

	render() {
		if (!this.state.render1 || !this.state.render2) {
			return (
				<LoaderCustom />
			)
		}
		console.log(this.state);


		return (
			<div>
				<Header 
					backRender={isBackRender(this.props, this.state)}
					handleBackButton={() => {this.props.history.goBack()}}
					handleLogout={this.handleLogout}
					handleLogoLink={() => {this.props.history.push('/monitoring/republic')}}
				/>
				<div className="container">
				    <MainPanel
				    	{...selectMainPanel(this.state)}
				    />
				    <ControlPanel 
				    	nameInput={this.state.filterNameInput}
				    	handleNameInput={(e) => { this.setState({ filterNameInput: e.target.value }) } }
				    	sortDropdown={this.state.filterSortDropdown}
				    	handleSortDropdown={(e) => { this.setState({ filterSortDropdown: e.target.value }) } }
				    />
				    <SecondaryPanelBlock
				    	secondaryPanelBlockArray={selectSecondaryPanelBlock(this.state, this.handlePanelClick)}
				    />
				</div>
			</div>
		)
	}
    
}

export default Monitoring;
