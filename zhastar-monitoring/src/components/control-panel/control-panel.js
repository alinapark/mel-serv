import React  from 'react';
import "./control-panel.css";

const ControlPanel = ({ handleNameInput, handleSortDropdown, nameInput, sortDropdown }) => (
    <div className="card control-panel monitoring__control-panel">
        <div className="card-content-small">
            <div className="control-panel__input-group">
                <div className="field control-panel__input">
                    <label className="label is-small">Название</label>
                    <div className="control">
                        <input className="input is-small" onChange={handleNameInput} value={nameInput} type="text" placeholder="Название"/>
                    </div>
                </div>
                <div className="field control-panel__input control-panel-input-group__control-panel">
                    <label className="label is-small">Сортировать по</label>
                    <div className="control">
                        <div className="select is-small" onChange={handleSortDropdown} value={sortDropdown} style={{width: "100%"}}>
                            <select style={{width: "100%"}}>
                                <option value=""></option>
                                <option value="alphabet"> По алфавиту </option>
                                <option value="early-det-desc"> По раннее выявление (убыв.) </option>
                                <option value="early-det-asc"> По раннее выявление (возр.) </option>
                                <option value="risk-group-desc"> По риск группе (убыв.) </option>
                                <option value="risk-group-asc"> По риск группе (возр.) </option>
                                <option value="suicide-success-desc"> По завершенным (убыв.) </option>
                                <option value="suicide-success-asc"> По завершенным (возр.) </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
)

export default ControlPanel;
