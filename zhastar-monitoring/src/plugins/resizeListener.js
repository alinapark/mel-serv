import centerDoughnutText from "./centerDoughnutText";
var w = window.innerWidth;

window.onresize = function() {

	if (w != window.innerWidth) {
		let canvases = document.getElementsByTagName("canvas");

		if (canvases.length > 0) {
			for (let i = 0; i < canvases.length; i++) {

				let width = window.getComputedStyle(canvases[i].parentElement.parentElement, null).getPropertyValue("width");
				let height = window.getComputedStyle(canvases[i].parentElement.parentElement, null).getPropertyValue("height");

				let canvasWidth = parseFloat(width.slice(0, -2)) / 3 - 40;
				let canvasWidthPx = canvasWidth + "px";

				let canvasHeight = parseFloat(height.slice(0, -2)) - 60;
				let canvasHeightPx = canvasHeight + "px";

				let squareSide = canvasWidth > canvasHeight ? canvasHeight : canvasWidth;
				let squareSideOffset = squareSide - 20;
				let squareSidePx = squareSideOffset + "px";

				canvases[i].style.width = canvasWidthPx;
				canvases[i].style.height = canvasHeightPx;
			}
		}

		centerDoughnutText();

		w = window.innerWidth;
	}
		
}
	